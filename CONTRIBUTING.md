# CONTRIBUTING

## Linting

Most guidelines are met through our linting setup, so make sure you have the necessary plugins (eslint, stylelint & prettier) enabled in your preferred code editor.

Commiting code will fail automatically if there is a linting problem in the introduced code.

## Commits

We follow the [conventional commits](https://www.conventionalcommits.org/en/v1.0.0-beta.2/) specification, so make sure your commits follow these guidelines.

Commiting by running `yarn cz` will provide an interactive cli tool that will generate the commit structure for you.

## Merge requests

No merge requests will be accepted if they have the `master` or `staging` branch as targets. Always use the `develop` branch as a target, unless you have a specific high-level feature branch in mind.

## Opening issues / bug reports

Issues are tracked in the Gitlab [issue tracker](https://gitlab.coko.foundation/micropubs/wormbase/issues) on this repository.  
This would also be the appropriate place to submit a feature request.

If you wish to report a bug, please keep the following structure:

- **_Steps_**: Instructions on how to reproduce the bug.
- **_Expected_**: Given a task, what you expected it to do.
- **_Actual_**: What happened instead.
- Add any extra comments you think clarify the issue or its context.

## Contact

Feel free to talk to us on [our chat channel](https://mattermost.coko.foundation/coko/channels/micropubs).
