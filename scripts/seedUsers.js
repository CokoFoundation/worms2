#! usr/bin/env node

/* eslint-disable sort-keys */
const pick = require('lodash/pick')
const range = require('lodash/range')
const uniqueId = require('lodash/uniqueId')
const { internet, name } = require('faker')

const logger = require('@pubsweet/logger')
const { Identity, Team, TeamMember, User } = require('@pubsweet/models')

const userData = [
  {
    email: 'author@example.com',
    givenNames: 'Author',
    surname: 'Authorius',
    username: 'author',
    password: 'password',
  },
  {
    email: 'editor@example.com',
    givenNames: 'Editor',
    surname: 'Editorius',
    username: 'editor',
    password: 'password',
  },
  {
    email: 'scienceOfficer@example.com',
    givenNames: 'ScienceOfficer',
    surname: 'ScienceOfficerius',
    username: 'scienceOfficer',
    password: 'password',
  },
  {
    email: 'reviewer@example.com',
    givenNames: 'Reviewer',
    surname: 'Reviewerius',
    username: 'reviewer',
    password: 'password',
  },
  {
    email: 'sectioneditor@example.com',
    givenNames: 'SectionEditor',
    surname: 'SectionEditorius',
    username: 'sectionEditor',
    password: 'password',
  },
  {
    email: 'curator@example.com',
    givenNames: 'Curator',
    surname: 'Curatorius',
    username: 'curator',
    password: 'password',
  },
]

const randomUsers = range(40).map(() => ({
  email: internet.email(),
  givenNames: name.firstName(),
  surname: name.lastName(),
  username: uniqueId('user-'),
  password: 'password',
}))

const roleToGlobalTeamMapper = {
  editor: 'editors',
  scienceOfficer: 'scienceOfficers',
  sectionEditor: 'globalSectionEditor',
  curator: 'globalCurator',
}

const assignGlobalUser = async (type, users) => {
  logger.info(`>>> Assigning global ${type}...`)

  const user = users.find(i => i.username === type)

  const team = await Team.query().findOne({
    global: true,
    role: roleToGlobalTeamMapper[type],
  })

  if (!team) {
    throw new Error('Assign global users: Team not found!')
  }

  const where = {
    teamId: team.id,
    userId: user.id,
  }
  const member = await TeamMember.query().findOne(where)

  if (member) {
    logger.info(
      `>>> User "${user.username}" has already been assigned to the ${type} team`,
    )
  }

  if (!member) {
    try {
      await TeamMember.query().insert(where)
      logger.info(`>>> Global ${type} successfully assigned`)
    } catch (e) {
      logger.error(e)
      process.kill(process.pid)
    }
  }
}

const seed = async () => {
  console.log('') /* eslint-disable-line no-console */
  logger.info('### CREATING FAKE USERS ###')
  logger.info('>>> Creating users...')

  const allUserData = [...userData, ...randomUsers]

  const newUsers = await Promise.all(
    allUserData.map(async data => {
      let identity, user

      identity = await Identity.query().findOne({
        email: data.email.toLowerCase(),
      })

      if (identity) {
        logger.info(`>>> User "${data.username}" already exists`)

        user = await User.query().findOne({
          username: data.username,
        })
      }

      if (!identity) {
        user = await User.query().insert({
          ...pick(data, ['givenNames', 'surname', 'username', 'password']),
          agreedTc: true,
        })

        identity = await Identity.query().insert({
          ...pick(data, ['email']),
          isConfirmed: true,
          isDefault: true,
          userId: user.id,
        })
      }

      return user
    }),
  )

  logger.info('>>> Users successfully created')

  await assignGlobalUser('editor', newUsers)
  await assignGlobalUser('scienceOfficer', newUsers)
  await assignGlobalUser('sectionEditor', newUsers)
  await assignGlobalUser('curator', newUsers)
}

seed()
