const { transaction } = require('objection')
const clone = require('lodash/clone')
const config = require('config')

const {
  CuratorReview,
  Manuscript,
  ManuscriptVersion,
  Review,
  Team,
  TeamMember,
} = require('@pubsweet/models')

const logger = require('@pubsweet/logger')

const { REVIEWER_STATUSES } = require('../constants')
const { notify } = require('../../services')
const useTransaction = require('../../models/_helpers/useTransaction')

const changeReviewerAutomationStatus = async (_, args, ctx) => {
  const { manuscriptVersionId, value } = args

  try {
    return useTransaction(async trx => {
      const updated = await ManuscriptVersion.query(trx).patchAndFetchById(
        manuscriptVersionId,
        {
          isReviewerAutomationOn: value,
        },
      )

      // If turning automation on
      if (value) {
        await updated.inviteMaxReviewers({ trx })
      }

      return updated
    })
  } catch (e) {
    logger.error(
      `Manuscript version resolver: Change reviewer automation status: ${e}`,
    )
    throw new Error(e)
  }
}

const manuscriptVersions = async (
  manuscript,
  { activeOnly, invitedToReviewOnly, last },
  ctx,
) => {
  const { user: userId } = ctx

  let query = ManuscriptVersion.query().orderBy('created', 'asc')

  const where = {
    manuscriptId: manuscript.id,
  }

  if (activeOnly) where.active = true
  if (last) query.limit(last)

  if (invitedToReviewOnly) {
    query = query
      .leftJoin('teams', 'teams.object_id', 'manuscript_versions.id')
      .leftJoin('team_members', 'team_members.team_id', 'teams.id')
      .whereIn('team_members.status', [
        REVIEWER_STATUSES.accepted,
        REVIEWER_STATUSES.rejected,
        REVIEWER_STATUSES.invited,
      ])

    where['teams.role'] = 'reviewer'
    where['team_members.userId'] = userId
  }

  const versions = await query.where(where)

  // TO DO -- this needs to change when we have more than one data type
  const restructured = versions.map(version => {
    const v = clone(version)
    v.geneExpression = version.dataTypeFormData && version.dataTypeFormData.data
    return v
  })

  return restructured
}

const manuscriptVersionImage = async (manuscriptVersion, args, ctx) => {
  const { image } = manuscriptVersion

  if (image && image.url) {
    const serverUrl =
      config.has('pubsweet-server.baseUrl') &&
      config.get('pubsweet-server.baseUrl')

    image.url = `${serverUrl}/uploads${image.url}`
  }

  return manuscriptVersion.image
}

const manuscriptVersionCuratorReviews = async (
  parent,
  { currentUserOnly },
  ctx,
) => {
  const { id: manuscriptVersionId } = parent
  const { user } = ctx

  const where = { manuscriptVersionId }
  if (currentUserOnly) where.curatorId = user

  return CuratorReview.query().where(where)
}

const saveSubmissionForm = async (_, { input }, ctx) => {
  const data = clone(input)

  const versionId = data.id
  delete data.id

  if (data.geneExpression) {
    data.dataTypeFormData = {
      data: data.geneExpression,
      type: 'geneExpression',
    }
    delete data.geneExpression
  }

  return ManuscriptVersion.query()
    .findById(versionId)
    .patch(data)
}

const setSOApproval = async (_, { manuscriptVersionId, approval }, ctx) => {
  try {
    await ManuscriptVersion.query()
      .patch({ isApprovedByScienceOfficer: approval })
      .findById(manuscriptVersionId)
      .throwIfNotFound()

    notify('scienceOfficerApprovalStatusChange', {
      versionId: manuscriptVersionId,
    })

    return approval
  } catch (e) {
    logger.error('Set SO approval: Update failed!')
    throw new Error(e)
  }
}

const submitDecision = async (_, { manuscriptVersionId, input }, ctx) => {
  const { user: userId } = ctx
  const { decision, decisionLetter } = input
  const accept = decision === 'accept'
  const decline = decision === 'decline'
  const reject = decision === 'reject'
  const revise = decision === 'revise'
  const publish = decision === 'publish'

  let version

  try {
    await transaction(ManuscriptVersion.knex(), async trx => {
      version = await ManuscriptVersion.query(trx).findById(manuscriptVersionId)

      if (version.decision)
        throw new Error('Submit decision: Version already has a decision!')

      await version.$query(trx).patch({
        decision,
        decisionLetter,
      })

      if (revise || accept) await version.makeNewVersion(trx)

      const notifyContext = { userId, version }
      if (accept) {
        const manuscript = await Manuscript.query(trx).findById(
          version.manuscriptId,
        )
        manuscript.history.accepted = new Date().toISOString()
        await manuscript.$query(trx).patch({ history: manuscript.history })

        notify('articleProof', notifyContext)
      }
      if (decline) notify('articleDeclined', notifyContext)
      if (reject) notify('articleRejected', notifyContext)
      if (revise) notify('articleRevision', notifyContext)
      if (publish) {
        const manuscript = await Manuscript.query(trx).findById(
          version.manuscriptId,
        )
        manuscript.history.published = new Date().toISOString()
        await manuscript.$query(trx).patch({ history: manuscript.history })

        notify('articlePublish', notifyContext)
      }
    })
    return version.id
  } catch (e) {
    logger.error('Submit decision: Transaction failed! Rolling back...')
    throw new Error(e)
  }
}

const submitManuscript = async (_, { input }, ctx) => {
  const { user: userId } = ctx
  const { id } = input
  const data = clone(input)

  const version = await ManuscriptVersion.query().findById(id)
  const manuscript = await Manuscript.query().findById(version.manuscriptId)
  const versions = await ManuscriptVersion.query()
    .where({
      manuscriptId: manuscript.id,
    })
    .orderBy('created')

  const { isInitiallySubmitted } = manuscript
  let updatedVersion

  try {
    await transaction(ManuscriptVersion.knex(), async trx => {
      if (version.submitted)
        throw new Error(
          'Submit manuscript: This version has already been submitted',
        )

      if (data.geneExpression) {
        data.dataTypeFormData = {
          data: data.geneExpression,
          type: 'geneExpression',
        }
        delete data.geneExpression
      }

      if (!isInitiallySubmitted) {
        await manuscript.$query(trx).patch({ isInitiallySubmitted: true })
      } else {
        data.submitted = true
      }

      updatedVersion = await version.$query(trx).patchAndFetch(data)

      // Send email
      const notificationContext = { userId, version: updatedVersion }

      if (!isInitiallySubmitted) {
        const received = new Date().toISOString()

        await manuscript.$query(trx).patch({
          history: {
            received,
          },
        })

        notify('initialSubmission', notificationContext)
      }
      if (isInitiallySubmitted && versions.length === 1)
        notify('fullSubmission', notificationContext)

      if (isInitiallySubmitted && versions.length > 1) {
        // If previous version has a review and `revisionReceived` hasn't been set yet
        if (!manuscript.history.revisionReceived) {
          const previousVersion = versions[versions.length - 2]
          const review = await Review.query().findOne({
            articleVersionId: previousVersion.id,
          })

          if (review && review.status.submitted) {
            manuscript.history.revisionReceived = new Date().toISOString()
            await manuscript.$query(trx).patch({ history: manuscript.history })
          }
        }

        notify('revisionSubmitted', notificationContext)
      }
    })

    return updatedVersion.id
  } catch (e) {
    logger.error('Submit manuscript: Transaction failed! Rolling back...')
    throw new Error(e)
  }
}

const validateReviewerPoolMember = async (userId, objectId, role) => {
  let model = Manuscript
  if (role === 'author') model = ManuscriptVersion

  const isMember = await model.hasRole(userId, objectId, role)

  if (isMember)
    throw new Error(`Cannot add ${role} with user id ${userId} as a reviewer`)
}

const updateReviewerPool = async (_, args) => {
  const { manuscriptVersionId, reviewerIds } = args

  try {
    // Reject membership if there are conflicts
    const manuscriptVersion = await ManuscriptVersion.query()
      .findById(manuscriptVersionId)
      .throwIfNotFound()

    const { manuscriptId } = manuscriptVersion

    await Promise.all(
      reviewerIds.map(async userId => {
        // authors cannot be reviewers of their own manuscript
        await validateReviewerPoolMember(userId, manuscriptVersionId, 'author')

        // these roles cannot be reviewers when assigned
        const manuscriptRolesToCheck = [
          'editor',
          'sectionEditor',
          'scienceOfficer',
          'curator',
        ]

        await Promise.all(
          manuscriptRolesToCheck.map(async role =>
            validateReviewerPoolMember(userId, manuscriptId, role),
          ),
        )
      }),
    )

    return useTransaction(async trx => {
      // update the array of ids that keeps the order
      const updated = await ManuscriptVersion.query(trx).patchAndFetchById(
        manuscriptVersionId,
        {
          reviewerPool: reviewerIds,
        },
      )

      // update team membership
      const reviewerTeam = await Team.query(trx).findOne({
        role: 'reviewer',
        objectId: manuscriptVersionId,
      })

      await Team.updateMembershipByTeamId(reviewerTeam.id, reviewerIds, { trx })

      return updated
    })
  } catch (e) {
    logger.error(`Manuscript version resolver: update reviewer pool: ${e}`)
    throw new Error(e)
  }
}

const reviewerPool = async (manuscriptVersion, args) => {
  const reviewerTeam = await Team.query()
    .findOne({
      objectId: manuscriptVersion.id,
      role: 'reviewer',
    })
    .throwIfNotFound()

  const reviewers = await Promise.all(
    manuscriptVersion.reviewerPool.map(reviewerId =>
      TeamMember.query().findOne({
        teamId: reviewerTeam.id,
        userId: reviewerId,
      }),
    ),
  )

  return reviewers
}

const changeAmountOfReviewers = async (_, { manuscriptVersionId, amount }) => {
  try {
    return ManuscriptVersion.query().patchAndFetchById(manuscriptVersionId, {
      amountOfReviewers: amount,
    })
  } catch (e) {
    logger.error(
      `Manuscript version resolver: Change amount of reviewers: Change failed!`,
    )
    throw new Error(e)
  }
}

module.exports = {
  changeAmountOfReviewers,
  changeReviewerAutomationStatus,
  manuscriptVersions,
  manuscriptVersionCuratorReviews,
  manuscriptVersionImage,
  reviewerPool,
  saveSubmissionForm,
  setSOApproval,
  submitDecision,
  submitManuscript,
  updateReviewerPool,
}
