// const moment = require('moment')

const { boss } = require('@coko/server')

const daysInSeconds = n => 60 * 60 * 24 * n

// const cronLater = addObject => {
//   const dateToSet = moment().add(addObject)

//   const minute = dateToSet.minute()
//   const hour = dateToSet.hour()
//   const day = dateToSet.date()
//   const month = dateToSet.month() + 1

//   const cronString = `${minute} ${hour} ${day} ${month} *`
//   return cronString
// }

// const cronDaysLater = n => {
//   const addObject = { days: n }
//   return cronLater(addObject)
// }

// const cronMinutesLater = n => {
//   const addObject = { minutes: n }
//   return cronLater(addObject)
// }

const deferJobByDays = async (daysToDefer, queueName, callback) => {
  await boss.publish(queueName, null, {
    startAfter: daysInSeconds(daysToDefer),

    // defer by tens of seconds (for testing)
    // startAfter: 10 * daysToDefer,
  })

  await boss.subscribe(queueName, {}, job => callback(job))
}

module.exports = {
  deferJobByDays,
}
