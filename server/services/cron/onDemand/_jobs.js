const { logger } = require('@coko/server')

const { deferJobByDays } = require('./_helpers')
const { REVIEWER_STATUSES } = require('../../../api/constants')
const useTransaction = require('../../../models/_helpers/useTransaction')
const TeamMember = require('../../../models/teamMember/teamMember.model')
const ManuscriptVersion = require('../../../models/manuscriptVersion/manuscriptVersion.model')
const notify = require('../../notify')

const remindReviewerJob = async ({
  name,
  isAccepted,
  teamMemberId,
  userId,
  versionId,
}) => {
  const days = isAccepted ? 3 : 5
  try {
    deferJobByDays(days, name, job => {
      notify('remindReviewer', {
        isAccepted,
        userId,
        versionId,
      })

      job.done()
    })
  } catch (e) {
    logger.error('Remind reviewer job error:', e)
  }
}

const revokeInvitationJob = async ({ name, teamMemberId, versionId }) => {
  await deferJobByDays(7, name, async job => {
    try {
      await useTransaction(async trx => {
        // revoke invitation
        await TeamMember.query()
          .patch({
            status: REVIEWER_STATUSES.revoked,
          })
          .findById(teamMemberId)
          .throwIfNotFound()

        // send email to reviewer
        // TODO: send email to editors
        notify('revokeInvitation', { teamMemberId, versionId })

        // run automation
        const version = await ManuscriptVersion.query().findById(versionId)
        await version.inviteMaxReviewers({ trx })
      })

      job.done()
    } catch (err) {
      logger.error(
        `Reviewer Invited: Automatic invitation revoke failed for team member ${teamMemberId}`,
      )
      job.done(err)
    }
  })
}

module.exports = { remindReviewerJob, revokeInvitationJob }
