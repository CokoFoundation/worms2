const { remindReviewerJob, revokeInvitationJob } = require('./_jobs')

// This will run on reviewer acceptance
const runReviewerAcceptedJobs = async context => {
  const { teamMemberId, userId, versionId } = context

  /**
   * If the reviewer hasn't left a review after 5 days, send reminder email.
   * If the reviewer hasn't left a review after 7 days, revoke invitation.
   * If the reviewer submits a review, these jobs need to be removed.
   */

  /* REMINDER */

  const remindName = `review-reminder-${teamMemberId}`

  await remindReviewerJob({
    name: remindName,
    isAccepted: true,
    teamMemberId,
    userId,
    versionId,
  })

  /* REVOKE INVITATION */

  const revokeName = `revoke-invitation-after-accept-${teamMemberId}`

  await revokeInvitationJob({
    name: revokeName,
    teamMemberId,
    versionId,
  })
}

module.exports = runReviewerAcceptedJobs
