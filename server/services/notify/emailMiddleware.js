/* eslint-disable global-require */

/**
 * NOTE:
 * Fix imports once all has been moved here from email service
 */

const cheerio = require('cheerio')
const { difference } = require('lodash')

// const logger = require('@pubsweet/logger')

const formatManuscriptTitle = str => {
  const $ = cheerio.load(str)
  /* eslint-disable-next-line no-param-reassign */
  const formatted = $('p').each((i, item) => (item.tagName = 'span'))
  return formatted
}

const getArticleUrl = manuscriptId => {
  const config = require('config')
  const baseUrl = config.get('pubsweet-server.baseUrl')
  return `${baseUrl}/article/${manuscriptId}`
}

const getArticleLink = manuscriptId => `
  <p>
    <a href="${getArticleUrl(manuscriptId)}">
      View it on the microPublication platform.
    </a>
  </p>
`

const getEmailsByUserIds = async userIds => {
  const { Identity, User } = require('@pubsweet/models')

  const users = await Promise.all(userIds.map(async id => User.findById(id)))

  const identities = await Promise.all(
    users.map(async user =>
      Identity.query().findOne({
        isDefault: true,
        userId: user.id,
      }),
    ),
  )

  const emails = identities.map(identity => identity.email).join(',')
  return emails
}

const getLastSubmittedVersion = async manuscriptId => {
  const { ManuscriptVersion } = require('@pubsweet/models')

  return ManuscriptVersion.query()
    .where({
      manuscriptId,
      submitted: true,
    })
    .orderBy('created', 'desc')
    .first()
}

const sendEmail = data => {
  const mailer = require('@pubsweet/component-send-email')
  const config = require('config')

  const { content, subject, to } = data

  const emailData = {
    from: config.get('mailer.from'),
    html: `<p>${content}</p>`,
    subject: `${subject}`,
    text: content,
    to,
  }

  mailer.send(emailData)
}

const unCamelCase = string =>
  string.replace(/([A-Z])/g, ' $1').replace(/^./, str => str.toUpperCase())

const updateManuscriptTeamMembership = async (
  resolve,
  parent,
  args,
  ctx,
  info,
) => {
  /**
   * input shape is
   * [
   *    {
   *      teamId: <team_id>,
   *      members: [<user_id>]
   *    }
   * ]
   */
  const { input } = args
  const Team = ctx.connectors.Team.model

  const teamIds = input.teams.map(item => item.teamId)

  // const manuscript = await Manuscript.query().findById()

  const teamQuery = () =>
    Team.query()
      .whereIn('teams.id', teamIds)
      .eager('users')

  // Get original team membership
  const originalTeams = await teamQuery()

  // Run resolver
  const response = await resolve(parent, args, ctx, info)

  // See what changed in team membership
  const newTeams = await teamQuery()
  const manuscriptId = newTeams[0].objectId
  const version = await getLastSubmittedVersion(manuscriptId)
  const articleLink = getArticleLink(manuscriptId)

  await Promise.all(
    newTeams.map(async newTeam => {
      const newUserIds = newTeam.users.map(user => user.id)
      const oldUserIds = originalTeams
        .find(t => t.id === newTeam.id)
        .users.map(user => user.id)

      const addedUserIds = difference(newUserIds, oldUserIds)

      const addedUserEmails = await getEmailsByUserIds(addedUserIds)

      if (addedUserEmails) {
        const role = unCamelCase(newTeam.role)
        const title = formatManuscriptTitle(version.title)

        const content = `
          <p>
            You have been assigned as ${role} for article "${title}".
          </p>
          ${articleLink}
        `

        const data = {
          content,
          subject: 'Role assignment',
          to: addedUserEmails,
        }

        sendEmail(data)
      }
    }),
  )

  return response
}

module.exports = { updateManuscriptTeamMembership }
