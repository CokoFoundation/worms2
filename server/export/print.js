const Ajv = require('ajv')
const beautify = require('js-beautify').html
const uniq = require('lodash/uniq')
const flatten = require('lodash/flatten')

const logger = require('@pubsweet/logger')

const errorMessage = 'HTML file creation:'

/* Helpers */

const contributionAuthor = name => /* html */ `
  <span data-id="author-contributions-item-name">
    ${name}: 
  </span>
`

const contributionCredit = credit => {
  const data = credit
    .map(c => unCamelCase(c))
    .map(item => contributionCreditItem(item))
    .join(', ')

  return data
}

const contributionCreditItem = text => text.toLowerCase().replace('_', ' - ')

const contributionItem = author =>
  `${contributionAuthor(author.name)}${contributionCredit(author.credit)}`

const getFieldValues = (list, field) => {
  const values = list.map(item => item[field])
  return values
}

const textSectionWithHeader = ({ data, label, name }) => {
  const textSection =
    data !== '<p></p>' && data !== ''
      ? `
  <div data-id="${name}">
    <h3 data-id="${name}-header">
      ${label}
    </h3>

    <div data-id="${name}-content">
      ${data}
    </div>
  </div>
`
      : ''
  return textSection
}

const unCamelCase = string =>
  string.replace(/([A-Z])/g, ' $1').replace(/^./, str => str.toUpperCase())

/* End Helpers */

/* Parts */
const abstractEl = abstract => {
  if (!abstract) {
    return ''
  }
  return textSectionWithHeader({
    data: abstract,
    label: 'Abstract',
    name: 'abstract',
  })
}

const acknowledgmentsEl = acknowledgments =>
  acknowledgments !== ''
    ? `<p><strong>Acknowledgments:</strong> ${acknowledgments}</p>`
    : ''

const authorContributionsEl = authors => {
  const data = authors.map(author => ({
    credit: author.credit,
    name: `${author.firstName} ${author.lastName}`,
  }))

  const items = data.map(item => contributionItem(item)).join('. ')

  return /* html */ `
    <p data-id="author-contributions-section">
      <strong>Author Contributions:</strong> ${items}.
    </p>
  `
}

const authorsEl = authors => {
  const affiliations = uniq(flatten(getFieldValues(authors, 'affiliations')))
  const affiliationsEls = affiliations
    .map(
      (affiliation, index) =>
        /* html */ `<div data-id="author-affiliation"><sup>${index +
          1}</sup>${affiliation}</div>`,
    )
    .join('')

  const nameEls = authors
    .map(author => {
      const correspondingAuthor = author.correspondingAuthor
        ? '<sup>&sect;</sup>'
        : ''

      const authorAffiliations = author.affiliations
        .map(authorAff => affiliations.findIndex(aff => authorAff === aff) + 1)
        .filter(number => number !== 0)
        .join(',')

      const equalContribution = author.equalContribution ? '<sup>*</sup>' : ''
      return /* html */ `<span data-id="author-name">${author.firstName} ${author.lastName}<sup>${authorAffiliations}</sup>${correspondingAuthor}${equalContribution}</span>`
    })
    .join(', ')

  const correspondingAuthors = authors
    .filter(author => author.correspondingAuthor)
    .map(author => author.email)
    .join('; ')
  const correspondence = `<sup>&sect;</sup>To whom correspondence should be addressed: ${correspondingAuthors}`

  const equalContributionAuthor = authors.findIndex(
    author => author.equalContribution,
  )
  const equalContribution =
    equalContributionAuthor !== -1
      ? '<sup>*</sup>These authors contributed equally.'
      : ''

  return /* html */ `
    <div data-id="author-section">
      <div data-id="author-names">
        ${nameEls}
      </div>
      <div data-id="author-affiliations">
        ${affiliationsEls}
      </div>
      <div>
        ${correspondence}
      </div>
      <div>
        ${equalContribution}
      </div>
    </div>
  `
}

const citationEl = (authors, date, title) => {
  const authorNames = authors
    .map(author => {
      const givenNames = author.firstName.split(' ')
      const initials = `${givenNames[0].substr(0, 1)}${
        givenNames[1] ? givenNames[1] : ''
      }`
      return `${author.lastName}, ${initials}`
    })
    .join('; ')

  const titleWithoutParagraph = title.replace('<p>', '').replace('</p>', '')

  const timestamp = new Date(Number(date))

  return `<p><strong>Citation:</strong> ${authorNames} (${timestamp.getFullYear()}), ${titleWithoutParagraph}. microPublication Biology.</p>`
}

const copyRight = date => {
  const timestamp = new Date(Number(date))
  const year = timestamp.getFullYear()

  return /* html */ `
  <p data-id="copyright-section">
    <span data-id="copyright-header">
      <strong>Copyright:</strong>
    </span>

    <span data-id="copyright-content"> 
      © ${year} by the authors. This is an open-access article distributed 
      under the terms of the Creative Commons Attribution 4.0 International 
      (CC BY 4.0) License, which permits unrestricted use, distribution, and 
      reproduction in any medium, provided the original author and source are 
      credited.
    </span>
  </p>
  `
}

const descriptionEl = description =>
  textSectionWithHeader({
    data: description,
    label: 'Description',
    name: 'description',
  })

const fundingEl = funding => /* html */ `
 <p><strong>Funding:</strong> ${funding}</p>`

const footer = date => {
  const timestamp = new Date(Number(date))
  const month = timestamp.getMonth() + 1
  const manuscriptDate = `${month}/${timestamp.getDate()}/${timestamp.getFullYear()}`

  return /* html */ `
              </div>
            </td>   
          </tr>
        </tbody>
        <tfoot>
          <tr>
            <td style="border: 0;">
              <div class="footer-space">&nbsp;</div>
            </td>
          </tr>
        </tfoot>
      </table>
      <div class="header"><img src="/logo.png" style="float: right; margin-right: 10px;" height="39"></div>

      <div class="header3">${manuscriptDate} - <em>Open Access</em></div>
    </body>
  </html>
`
}

const header = /* html */ `
  <html>
    <head>
      <link rel="stylesheet" type="text/css" href="/print.css"/>
    </head>

    <body>
    <table>
      <thead>
        <tr>
          <td style="border: 0;">
            <div class="header-space">&nbsp;</div>
          </td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="border: 0;">
            <div class="content">
`

const imageEl = (imageSrc, imageTitle, imageCaption) => /* html */ `
  <div data-id="image-section">
    <img data-id="image" src="${imageSrc}" />
  </div>
  <div class="figure">
    <p><b>${imageTitle}</b></p>
    ${imageCaption}
  </div>
`

const methodsEl = methods =>
  textSectionWithHeader({
    data: methods,
    label: 'Methods',
    name: 'methods',
  })

const reagentsEl = reagents =>
  textSectionWithHeader({
    data: reagents,
    label: 'Reagents',
    name: 'reagents',
  })

const referencesEl = references => {
  const referencesString = references
    .map((ref, index) => {
      const { reference } = ref
      const doi =
        ref.doi === ''
          ? '<span class="warning">Please supply DOI or PubMed ID if possible.</span>'
          : `<a href="https://doi.org/${ref.doi}">${ref.doi}</a>`
      const pubmedId =
        ref.pubmedId === ''
          ? null
          : `<a href="https://www.ncbi.nlm.nih.gov/pubmed/${ref.pubmedId}">PubMed</a>`

      const referenceId = pubmedId || doi
      return `<div data-id="reference-${index}" class="reference">${reference} ${referenceId}</div>`
    })
    .join('</p><p>')

  return textSectionWithHeader({
    data: `<p>${referencesString}</p>`,
    label: 'References',
    name: 'references',
  })
}

const titleEl = title => /* html */ `
  <h1 data-id="title">
    ${title}
  </h1>
`
/* End Parts */

/* 
  Create complete HTML string
  reviewed by -- don't have name
  received / accepted / published online -- don't have dates!
  citation -- don't have author first/last names
*/
const createHTML = (manuscript, imageSrc) => `
  ${header}
  ${titleEl(manuscript.title)}
  ${authorsEl(manuscript.authors)}
  ${abstractEl(manuscript.abstract)}
  ${imageEl(imageSrc, manuscript.imageTitle, manuscript.imageCaption)}
  ${descriptionEl(manuscript.patternDescription)}
  ${methodsEl(manuscript.methods)}
  ${reagentsEl(manuscript.reagents)}
  ${acknowledgmentsEl(manuscript.acknowledgements)}
  ${referencesEl(manuscript.references)}
  ${fundingEl(manuscript.funding)}
  ${authorContributionsEl(manuscript.authors)}
  ${copyRight(manuscript.updated)}
  ${citationEl(manuscript.authors, manuscript.updated, manuscript.title)}
  ${footer(manuscript.updated)}
`

/* Validation */
/* eslint-disable sort-keys */
const stringNotEmpty = {
  type: 'string',
  minLength: 1,
}

const arrayOfStringsNotEmpty = {
  type: 'array',
  items: stringNotEmpty,
  minItems: 1,
}

const arrayNotEmpty = keys => ({
  ...keys,
  type: 'array',
  minItems: 1,
})

const author = {
  type: 'object',
  properties: {
    affiliations: arrayOfStringsNotEmpty,
    credit: arrayOfStringsNotEmpty,
    lastName: stringNotEmpty,
  },
  required: ['affiliations', 'credit', 'lastName'],
}

const authors = arrayNotEmpty({ items: author })

const schema = {
  type: 'object',
  properties: {
    authors,
    funding: stringNotEmpty,
    imageCaption: stringNotEmpty,
    patternDescription: stringNotEmpty,
    title: stringNotEmpty,
  },
  required: [
    'authors',
    'funding',
    'imageCaption',
    'patternDescription',
    'title',
  ],
}
/* eslint-enable sort-keys */

const validate = (manuscript, imageSrc) => {
  const errorText = `${errorMessage} Validate:`
  if (!imageSrc) {
    logger.error(`${errorText} No image src string provided`)
    return false
  }

  const ajv = new Ajv()
  const valid = ajv.validate(schema, manuscript)
  if (!valid) logger.error(`${errorText} ${ajv.errorsText()}`)

  return valid
}
/* End Validation */

const output = (manuscript, imageSrc) => {
  const valid = validate(manuscript, imageSrc)
  if (!valid) return null

  const html = createHTML(manuscript, imageSrc)
  return beautify(html)
}

module.exports = output
