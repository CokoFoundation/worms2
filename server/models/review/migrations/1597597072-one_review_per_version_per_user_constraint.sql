CREATE UNIQUE INDEX one_review_per_version_per_user
ON reviews (article_version_id, reviewer_id)
;
