const { transaction } = require('objection')
const { isEqual, uniqWith } = require('lodash')

const {
  ChatMessage,
  ChatThread,
  Manuscript,
  TeamMember,
} = require('@pubsweet/models')
const logger = require('@pubsweet/logger')

/*
  All communicationHistory columns on existing manuscripts are equivalent
  to the new ChatThread of type 'scienceOfficer'.
*/

exports.up = async knex => {
  try {
    await transaction(Manuscript.knex(), async trx => {
      const manuscripts = await Manuscript.query(trx).select(
        'id',
        'communicationHistory',
      )

      await Promise.all(
        manuscripts.map(async manuscript => {
          // Create SO chat
          const SOThread = await ChatThread.query(trx).insert({
            chatType: 'scienceOfficer',
            manuscriptId: manuscript.id,
          })

          // Create author chat
          await ChatThread.query(trx).insert({
            chatType: 'author',
            manuscriptId: manuscript.id,
          })

          // Create reviewer chats for each accepted reviewer
          const acceptedReviewers = await TeamMember.query(trx)
            .leftJoin('teams', 'team_members.team_id', 'teams.id')
            .leftJoin(
              'manuscript_versions',
              'teams.object_id',
              'manuscript_versions.id',
            )
            .leftJoin(
              'manuscripts',
              'manuscript_versions.manuscript_id',
              'manuscripts.id',
            )
            .select(
              'teams.role',
              'team_members.status',
              'team_members.user_id as reviewerId',
              'manuscripts.id as manuscriptId',
            )
            .where({
              manuscriptId: manuscript.id,
              role: 'reviewer',
              status: 'acceptedInvitation',
            })

          const reviewerChatData = acceptedReviewers.map(item => {
            const { manuscriptId, reviewerId } = item
            return {
              chatType: 'reviewer',
              manuscriptId,
              reviewerId,
            }
          })

          const duplicateFreeReviewerChatData = uniqWith(
            reviewerChatData,
            isEqual,
          )
          await ChatThread.query(trx).insert(duplicateFreeReviewerChatData)

          // Populate SO chat with messages
          if (
            !Array.isArray(manuscript.communicationHistory) ||
            manuscript.communicationHistory.length === 0
          ) {
            return
          }

          const messageData = manuscript.communicationHistory.map(message => {
            const { content, timestamp, user } = message

            return {
              content,
              chatThreadId: SOThread.id,
              timestamp,
              userId: user,
            }
          })

          await ChatMessage.query(trx).insert(messageData)
        }),
      )
    })
  } catch (e) {
    logger.error('Chat Thread: Move communication history: Migration failed!')
    throw new Error(e)
  }
}
