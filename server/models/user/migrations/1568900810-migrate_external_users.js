const { transaction } = require('objection')

const { Identity, User } = require('@pubsweet/models')
const logger = require('@pubsweet/logger')

const baseMessage = 'Migrate external users:'

exports.up = async knex => {
  let externalUsers = []

  try {
    externalUsers = await knex('external_user')
  } catch (err) {
    logger.warn(
      `${baseMessage} Failed to get external users. Most likely table "external_user" does not exist`,
      // err,
    )
  }

  await Promise.all(
    externalUsers.map(async user => {
      const userData = {
        id: user.id,
        givenNames: user.name
          .split(' ')
          .slice(0, -1)
          .join(' '),
        surname: user.name.split(' ').pop(),
      }

      try {
        await transaction(User.knex(), async trx => {
          const exists = await Identity.query(trx).findOne({
            email: user.email,
          })

          if (exists) {
            logger.error(`${baseMessage} Identity already exists`)
          } else {
            const newUser = await User.query(trx).insert({
              ...userData,
              agreedTc: false,
            })

            await Identity.query(trx).insert({
              email: user.email,
              isConfirmed: true,
              isDefault: true,
              userId: newUser.id,
            })
          }
        })

        await knex('external_user')
          .where({
            id: user.id,
          })
          .del()
      } catch (e) {
        logger.error(`${baseMessage} Transaction failed! Rolling back...`)
        throw new Error(e)
      }
    }),
  )
}
