const { internet, name } = require('faker')
const range = require('lodash/range')

const Identity = require('../../identity/identity.model')
const User = require('../../user/user.model')

const createUser = async () => {
  const user = await User.query().insert({
    givenNames: name.firstName(),
    surname: name.lastName(),
  })

  await Identity.query().insert({
    userId: user.id,
    email: internet.email(),
    isConfirmed: true,
    isDefault: true,
  })

  return user
}

const createUsers = async n => Promise.all(range(n).map(() => createUser()))

module.exports = {
  createUser,
  createUsers,
}
