alter table only manuscripts

drop column acknowledgements,
drop column authors,
drop column comments,
drop column disclaimer,
drop column funding,
drop column gene_expression,
drop column image,
drop column image_caption,
drop column laboratory,
drop column methods,
drop column pattern_description,
drop column "references",
drop column suggested_reviewer,
drop column title,

drop column decision_letter
;