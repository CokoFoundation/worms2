const { Manuscript, Team } = require('@pubsweet/models')
const logger = require('@pubsweet/logger')
const { transaction } = require('objection')

const baseMessage = 'Migrations: Add Curator Teams:'

exports.up = async knex => {
  try {
    await transaction(Manuscript.knex(), async trx => {
      const manuscripts = await Manuscript.query(trx)

      const teamDataToInsert = manuscripts.map(manuscript => ({
        name: `curator-${manuscript.id}`,
        objectId: manuscript.id,
        objectType: 'article',
        role: 'curator',
      }))

      await Team.query(trx).insert(teamDataToInsert)
    })
  } catch (e) {
    logger.error(`${baseMessage} Migration failed! Rolling back...`)
    throw new Error(e)
  }
}
