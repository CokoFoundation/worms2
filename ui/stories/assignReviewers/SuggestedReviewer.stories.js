import React from 'react'
import { name } from 'faker'

import SuggestedReviewer from '../../src/assignReviewers/SuggestedReviewer'

export const Base = () => <SuggestedReviewer name={name.findName()} />

export default {
  component: SuggestedReviewer,
  title: 'Assign Reviewers/SuggestedReviewer',
}
