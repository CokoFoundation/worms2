import React from 'react'
import styled from 'styled-components'
import { range } from 'lodash'
import { lorem, name, random } from 'faker'

import SearchBox from '../../src/assignReviewers/SearchBox'

const Wrapper = styled.div`
  height: 400px;
`

const people = range(40).map(() => ({
  value: random.uuid(),
  label: name.findName(),
  isDisabled: random.boolean(),
  status: random.arrayElement([null, lorem.words(random.number(2))]),
}))

export const Base = () => {
  const search = input =>
    new Promise(resolve =>
      setTimeout(() => {
        const typed = input && input.toLowerCase()
        const results = people.filter(person => {
          const fullName = person.label.toLowerCase()
          return fullName.includes(typed)
        })

        resolve(results)
      }, 1000),
    )

  return (
    <Wrapper>
      <SearchBox
        onClickOption={val => console.log(val)}
        placeholder="Search for someone"
        search={search}
      />
    </Wrapper>
  )
}

export default {
  component: SearchBox,
  title: 'Assign Reviewers/Search Box',
}
