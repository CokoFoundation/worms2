import React from 'react'
// import { lorem } from 'faker'

import { Button } from '../../src/common'
import Password from '../../src/userProfile/Password'
import UserProfileSection from '../../src/userProfile/UserProfileSection'

export const Base = () => (
  <UserProfileSection
    headerText="Some section"
    successMessage="Success!"
    update={() => Promise.resolve()}
  >
    {update => (
      <Button onClick={update} primary>
        Click me for success
      </Button>
    )}
  </UserProfileSection>
)

export const PasswordAsChild = () => (
  <UserProfileSection
    headerText="Password"
    successMessage="Password successfully updated"
    update={() => Promise.resolve()}
  >
    {update => <Password update={update} />}
  </UserProfileSection>
)

// export const PasswordThatHasError = () => (
//   <UserProfileSection
//     headerText="Password"
//     successMessage="Password successfully updated"
//     update={() => Promise.reject()}
//   >
//     {update => <Password update={update} />}
//   </UserProfileSection>
// )

export default {
  component: UserProfileSection,
  title: 'User Profile/Section',
}
