import React from 'react'
import { internet, name, random } from 'faker'

import UserProfile from '../../src/userProfile/UserProfile'

const resolve = () => Promise.resolve()

export const Base = () => (
  <UserProfile
    givenNames={name.firstName()}
    orcid={random.uuid()}
    surname={name.lastName()}
    updatePassword={resolve}
    updatePersonalInformation={resolve}
    updateUsername={resolve}
    username={internet.userName()}
  />
)

export const Loading = () => <UserProfile loading />

export default {
  component: UserProfile,
  title: 'User Profile/User Profile',
}
