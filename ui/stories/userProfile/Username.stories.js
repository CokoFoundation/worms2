import React from 'react'
import { internet } from 'faker'

import Username from '../../src/userProfile/Username'

export const Base = () => (
  <Username
    update={vals => console.log('update!', vals)}
    username={internet.userName()}
  />
)

export default {
  component: Username,
  title: 'User Profile/Username',
}
