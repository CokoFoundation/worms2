import React, { useState } from 'react'

import TabsLock from '../../src/split/TabsLock'

export const Base = () => {
  const [locked, setLocked] = useState(true)

  return <TabsLock locked={locked} onClick={() => setLocked(!locked)} />
}

export default {
  component: TabsLock,
  title: 'Article/Tabs Lock',
}
