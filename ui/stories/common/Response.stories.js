import React from 'react'
import { lorem } from 'faker'

import Response from '../../src/common/Response'

export const Base = () => <Response icon="box">{lorem.sentences(2)}</Response>

export const Success = () => (
  <Response icon="check_circle" status="success">
    {lorem.sentences(2)}
  </Response>
)

export const Error = () => (
  <Response icon="x_circle" status="error">
    {lorem.sentences(2)}
  </Response>
)

export const Warning = () => (
  <Response icon="alert_circle" status="warning">
    {lorem.sentences(2)}
  </Response>
)

export default {
  component: Response,
  title: 'Common/Response',
}
