import React from 'react'

import Label from '../../src/preview/Label'

export const Base = () => <Label value="This is the label" />

export default {
  component: Label,
  title: 'Preview/Label',
}
