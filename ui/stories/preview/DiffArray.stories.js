import React from 'react'

import Author from '../../src/preview/Author'
import DiffArray from '../../src/preview/DiffArray'

export const Base = () => (
  <DiffArray currentArray={['two', 'three']} previousArray={['one', 'two']} />
)

export const Numbers = () => (
  <DiffArray currentArray={[2, 3]} previousArray={[1, 2]} />
)

const objects = [
  {
    id: 1,
    value: 'first value',
  },
  {
    id: 2,
    value: 'second value',
  },
  {
    id: 3,
    value: 'third value',
  },
]

export const Objects = () => (
  <DiffArray
    currentArray={objects.slice(1)}
    previousArray={objects.slice(0, 2)}
  />
)

const authors = [
  {
    affiliations: [1],
    id: 1,
    isCorresponding: true,
    name: 'John Smith',
  },
  {
    affiliations: [1, 2],
    id: 2,
    isCorresponding: false,
    name: 'John Smith',
  },
  {
    affiliations: [3],
    id: 3,
    isCorresponding: true,
    name: 'John Robertson',
  },
]

export const AuthorsCustomComponent = () => (
  <DiffArray
    component={Author}
    currentArray={authors.slice(1)}
    previousArray={authors.slice(0, 2)}
  />
)

export const HideRemovedOnly = () => (
  <DiffArray
    component={Author}
    currentArray={authors.slice(1)}
    previousArray={authors.slice(0, 2)}
    showRemoved={false}
  />
)

export const HideAllDiffs = () => (
  <DiffArray
    component={Author}
    currentArray={authors.slice(1)}
    previousArray={authors.slice(0, 2)}
    showDiff={false}
  />
)

export default {
  component: DiffArray,
  title: 'Preview/Diff Array',
}
