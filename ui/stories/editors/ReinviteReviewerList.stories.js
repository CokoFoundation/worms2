import React from 'react'
import { name, random } from 'faker'
import { range } from 'lodash'

import ReinviteReviewerList from '../../src/editors/ReinviteReviewerList'

const makeReviewers = amount =>
  range(amount).map(() => ({
    displayName: name.findName(),
    id: random.uuid(),
    recommendation: random.arrayElement(['accept', 'reject', 'revise']),
  }))

export const Base = () => (
  <ReinviteReviewerList
    onClickInvite={id => console.log('invite', id)}
    reviewers={makeReviewers(3)}
  />
)

export const Empty = () => <ReinviteReviewerList />

export default {
  component: ReinviteReviewerList,
  title: 'Editors/Reinvite Reviewer List',
}
