/* eslint-disable react/prop-types */

import React from 'react'
import styled from 'styled-components'
import { lorem } from 'faker'

import { th } from '@pubsweet/ui-toolkit'

const Root = styled.div`
  margin-bottom: 10px;
  padding: 10px;
  text-align: justify;
`

const InterfaceText = styled(Root)`
  font-family: ${th('fontInterface')};
`

const ReadingText = styled(Root)`
  font-family: ${th('fontReading')};
`

const ColorText = styled(InterfaceText)`
  color: ${th('colorText')};
`

const ColorTextPlaceholder = styled(InterfaceText)`
  color: ${th('colorTextPlaceholder')};
`

const ColorTextReverse = styled(InterfaceText)`
  background-color: ${th('colorTextPlaceholder')};
  color: ${th('colorTextReverse')};
`

const ColorPrimary = styled(InterfaceText)`
  color: ${th('colorPrimary')};
`

const ColorSuccess = styled(InterfaceText)`
  color: ${th('colorSuccess')};
`

const ColorError = styled(InterfaceText)`
  color: ${th('colorError')};
`

const ColorWarning = styled(InterfaceText)`
  color: ${th('colorWarning')};
`

const SizeBase = styled(InterfaceText)`
  font-size: ${th('fontSizeBase')};
  line-height: ${th('lineHeightBase')};
`

const SizeBaseSmall = styled(InterfaceText)`
  font-size: ${th('fontSizeBaseSmall')};
  line-height: ${th('lineHeightBaseSmall')};
`

const Label = styled.div`
  border-bottom: 2px solid ${th('colorTextPlaceholder')};
  color: ${th('colorTextPlaceholder')};
  font-size: ${th('fontSizeBaseSmall')};
  margin-bottom: 16px;
`

const Display = props => {
  const { label, component: Component } = props
  return (
    <>
      <Label>{label}</Label>
      <Component>{lorem.sentences(7)}</Component>
    </>
  )
}

export const Base = () => (
  <>
    <Display component={InterfaceText} label="fontInterface" />
    <Display component={ReadingText} label="fontReading" />
  </>
)

export const Colors = () => (
  <>
    <Display component={ColorText} label="colorText" />
    <Display component={ColorTextPlaceholder} label="colorTextPlaceholder" />
    <Display component={ColorTextReverse} label="colorTextReverse" />
    <Display component={ColorPrimary} label="colorPrimary" />
    <Display component={ColorSuccess} label="colorSuccess" />
    <Display component={ColorError} label="colorError" />
    <Display component={ColorWarning} label="colorWarning" />
  </>
)

export const Sizes = () => (
  <>
    <Display component={SizeBase} label="fontSizeBase" />
    <Display component={SizeBaseSmall} label="fontSizeBaseSmall" />
  </>
)

export default {
  title: 'Basics/Typography',
}
