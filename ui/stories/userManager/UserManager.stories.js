import React from 'react'
import { internet, name, random } from 'faker'
import { range } from 'lodash'

import UserManager from '../../src/userManager/UserManager'

const updateUserData = input => Promise.resolve(input)

const users = range(10).map(() => ({
  admin: random.boolean(),
  displayName: name.findName(),
  email: internet.email(),
  givenNames: name.firstName(),
  surname: name.lastName(),
  username: internet.userName(),
  updateUserData,
}))

export const Base = () => <UserManager users={users} />

export default {
  component: UserManager,
  title: 'User Manager/User Manager',
}
