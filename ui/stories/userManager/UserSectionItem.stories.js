import React from 'react'
import { internet, name, random } from 'faker'

import UserSectionItem from '../../src/userManager/UserSectionItem'

const updateUserData = input => Promise.resolve(input)

const user = {
  admin: random.boolean(),
  displayName: name.findName(),
  email: internet.email(),
  givenNames: name.firstName(),
  surname: name.lastName(),
  username: internet.userName(),
}

export const Base = () => (
  <UserSectionItem
    admin={user.admin}
    displayName={user.displayName}
    email={user.email}
    givenNames={user.givenNames}
    surname={user.surname}
    updateUserData={updateUserData}
    username={user.username}
  />
)

export default {
  component: UserSectionItem,
  title: 'User Manager/User Section Item',
}
