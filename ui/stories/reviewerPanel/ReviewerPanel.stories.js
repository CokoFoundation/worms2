import React, { useState } from 'react'
import styled from 'styled-components'
import { lorem, random } from 'faker'

import ReviewerPanel from '../../src/reviewerPanel/ReviewerPanel'

const Demo = styled.div`
  border-bottom: 2px solid gray;
  margin-bottom: 20px;
  padding-bottom: 10px;
`

const makeCuratorOptions = theme => [
  {
    color: theme.colorSuccess,
    label: 'Meets DB standards',
    value: 'accept',
  },
  {
    color: theme.colorWarning,
    label: 'Needs modification to meet DB standards',
    value: 'revise',
  },
  {
    color: theme.colorError,
    label: 'I have major concerns with this article',
    value: 'reject',
  },
]

const makeReviewerOptions = theme => [
  {
    color: theme.colorSuccess,
    label: 'Accept version',
    value: 'accept',
  },
  {
    color: theme.colorWarning,
    label: 'Needs revision',
    value: 'revise',
  },
  {
    color: theme.colorError,
    label: 'Reject ',
    value: 'reject',
  },
]

export const Base = () => {
  const [review, setReview] = useState(null)
  const [submitted, setSubmitted] = useState(false)

  const save = values => {
    console.log('autosave running', values)
  }

  const submit = values => {
    console.log('submitting!', values)
    setReview(values)
    setSubmitted(true)
  }

  const reset = () => {
    setReview(null)
    setSubmitted(false)
  }

  return (
    <>
      <Demo>
        For demo purposes only:
        <button disabled={!submitted} onClick={reset}>
          Unsubmit form
        </button>
      </Demo>

      <ReviewerPanel
        makeOptions={makeReviewerOptions}
        onClickChat={() => console.log('open chat')}
        review={review}
        save={save}
        submit={submit}
        submitted={submitted}
      />
    </>
  )
}

export const CuratorPanel = () => (
  <ReviewerPanel
    dbReferenceId="DBReferenceID"
    doi="10.12345/publisher.123456"
    makeOptions={makeCuratorOptions}
    onClickChat={() => console.log('open chat')}
    review={review}
    reviewTextAreaLabel="Curator review text"
    reviewTextAreaPlaceholder="Enter your curator feedback here"
    save={vals => console.log('save!', vals)}
    showRequestToSeeRevision={false}
    submit={vals => console.log('submit!', vals)}
  />
)

const validRecommendations = ['accept', 'reject', 'revise']

const review = {
  content: lorem.sentences(30),
  recommendation: random.arrayElement(validRecommendations),
  openAcknowledgement: random.boolean(),
  askedToSeeRevision: random.boolean(),
}

export const Submitted = () => (
  <ReviewerPanel makeOptions={makeReviewerOptions} review={review} submitted />
)

export const SubmittedAndDecisionExists = () => (
  <ReviewerPanel
    decision="revise"
    makeOptions={makeReviewerOptions}
    review={review}
    submitted
  />
)

export const TooLateToSubmit = () => (
  <ReviewerPanel
    decision="revise"
    makeOptions={makeReviewerOptions}
    submtited={false}
  />
)

export default {
  component: ReviewerPanel,
  title: 'Reviewer & Curator Panel/Reviewer Panel',
}
