import React, { useState } from 'react'
import { name, random } from 'faker'
import { range } from 'lodash'

import TeamSection from '../../src/teamManager/TeamSection'

const users = range(10).map(() => ({
  displayName: name.findName(),
  id: random.uuid(),
}))

export const Base = () => {
  // fake formik state
  const [values, setValues] = useState([])
  const handleChange = vals => setValues(vals)

  return (
    <TeamSection
      displayName="Editors"
      handleChange={handleChange}
      name="editors"
      users={users}
      value={values}
    />
  )
}

export default {
  component: TeamSection,
  title: 'Team Manager/Team Section',
}
