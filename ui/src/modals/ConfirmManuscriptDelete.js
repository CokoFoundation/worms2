import React from 'react'
import PropTypes from 'prop-types'

import WarningModal from './WarningModal'
import { stripHTML } from '../_helpers'

const ConfirmManuscriptDelete = props => {
  const { title, ...rest } = props

  return (
    <WarningModal headerText="confirm delete" textSuccess="delete" {...rest}>
      {`You are about to delete the article "${stripHTML(
        title,
      )}". Are you sure?`}
    </WarningModal>
  )
}

ConfirmManuscriptDelete.propTypes = {
  title: PropTypes.string.isRequired,
}

export default ConfirmManuscriptDelete
