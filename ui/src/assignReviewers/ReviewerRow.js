import React from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'

import { Action, ActionGroup, Icon, Status } from '../common'
import { grid, th } from '../_helpers'

const draggingStyles = css`
  box-shadow: 0 0 0 2px ${th('colorPrimary')};
`

const Wrapper = styled.div`
  background: ${th('colorBackground')};
  box-shadow: 0 0 0 1px ${th('colorPrimary')};
  display: flex;
  flex-direction: column;
  padding: ${grid(1)};
  transition: box-shadow 0.1s ease-in;

  &:hover {
    ${draggingStyles}
  }

  /* stylelint-disable-next-line order/order, order/properties-alphabetical-order */
  ${props => props.isDragging && draggingStyles}
`

const TopRow = styled.div`
  display: flex;
  justify-content: space-between;
`

const LeftSide = styled.div``

const RightSide = styled.div`
  display: flex;
`

const StatusWrapper = styled.div`
  display: inline-block;
  margin-left: ${grid(2)};
`

const StyledStatus = styled(Status)`
  font-size: 12px;
  margin: 0 4px;
`

const IconButton = styled(Icon)`
  &:hover {
    background: ${th('colorSecondary')};
    cursor: pointer;
  }
`

const EmailWrapper = styled.div`
  display: inline-block;
  font-size: ${th('fontSizeBaseSmall')};
  margin-left: ${grid(2)};
  /* padding-top: ${grid(0.5)}; */
`

const EmailLabel = styled.span`
  color: ${th('colorPrimary')};
  margin-right: ${grid(1)};
  text-transform: uppercase;
`

const ReviewerRow = props => {
  const {
    canInvite,
    className,
    displayName,
    email,
    invited,
    isDragging,
    isSignedUp,
    acceptedInvitation,
    rejectedInvitation,
    invitationRevoked,
    onClickInvite,
    onClickRemove,
    onClickRevokeInvitation,
    reviewerId,
    reviewSubmitted,
    innerRef,
    showEmail,
    ...rest
  } = props

  const reviewPending = invited && acceptedInvitation && !reviewSubmitted
  const submitted = invited && acceptedInvitation && reviewSubmitted
  const rejected = invited && rejectedInvitation
  const revoked = invited && invitationRevoked
  const responsePending =
    invited && !invitationRevoked && !acceptedInvitation && !rejectedInvitation
  const notInvited = !invited && !invitationRevoked

  const makeStatusText = () => {
    if (reviewPending) return 'accepted invitation - pending review'
    if (submitted) return 'review submitted'
    if (rejected) return 'rejected invitation'
    if (revoked) return 'invitation revoked'
    if (responsePending) return 'invited'
    if (notInvited) return 'not invited'
    return null
  }

  const makeStatus = () => {
    if (reviewPending) return 'success'
    if (submitted) return 'primary'
    if (rejected) return 'error'
    if (revoked) return 'error'
    if (responsePending) return 'success'
    if (notInvited) return null
    return null
  }

  return (
    <Wrapper
      className={className}
      isDragging={isDragging}
      ref={innerRef}
      {...rest}
    >
      <TopRow>
        <LeftSide>
          {displayName}
          <StatusWrapper>
            <StyledStatus reverseColors status={makeStatus()}>
              {makeStatusText()}
            </StyledStatus>

            {!isSignedUp && (
              <StyledStatus reverseColors>Not signed up</StyledStatus>
            )}
          </StatusWrapper>

          {showEmail && (
            <EmailWrapper>
              <EmailLabel>Email:</EmailLabel>
              {email}
            </EmailWrapper>
          )}
        </LeftSide>

        <RightSide>
          {notInvited && (
            <>
              <ActionGroup>
                <Action disabled={!canInvite} onClick={onClickInvite}>
                  Invite
                </Action>
              </ActionGroup>
              <IconButton onClick={onClickRemove} size={2}>
                x
              </IconButton>
            </>
          )}

          {revoked && (
            <ActionGroup>
              <Action disabled={!canInvite} onClick={onClickInvite}>
                Reinvite
              </Action>
            </ActionGroup>
          )}

          {responsePending && (
            <ActionGroup>
              <Action onClick={onClickRevokeInvitation}>
                Revoke invitation
              </Action>
            </ActionGroup>
          )}
        </RightSide>
      </TopRow>
    </Wrapper>
  )
}

ReviewerRow.propTypes = {
  /** Whether this reviewer can be invited */
  canInvite: PropTypes.bool.isRequired,
  /** Reviwer's display name */
  displayName: PropTypes.string.isRequired,
  /** Reviewer's email */
  email: PropTypes.string.isRequired,
  /** Row's innerRef. For use by dnd */
  innerRef: PropTypes.func.isRequired,
  /** Whether this reviewer has been invited */
  invited: PropTypes.bool.isRequired,
  /** Whether this row is being dragged */
  isDragging: PropTypes.bool.isRequired,
  /** Whether the user is signed up or not */
  isSignedUp: PropTypes.bool.isRequired,
  /** Whether the reviewer has accepted the invitation */
  acceptedInvitation: PropTypes.bool.isRequired,
  /** Whether the reviewer has rejected the invitation */
  rejectedInvitation: PropTypes.bool.isRequired,
  /** Whether the invitation has been revoked */
  invitationRevoked: PropTypes.bool.isRequired,
  /** The reviewer's id */
  reviewerId: PropTypes.string.isRequired,
  /** Whether the reviewer submitted their review */
  reviewSubmitted: PropTypes.bool.isRequired,
  /** Function to run when clicking 'invite' */
  onClickInvite: PropTypes.func.isRequired,
  /** Function to run on clicking 'X' */
  onClickRemove: PropTypes.func.isRequired,
  /** Function to run on clicking 'revoke invitation' */
  onClickRevokeInvitation: PropTypes.func.isRequired,
  /** Display reviewer's email */
  showEmail: PropTypes.bool,
}

ReviewerRow.defaultProps = {
  showEmail: false,
}

export default ReviewerRow
