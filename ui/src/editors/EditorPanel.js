import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { grid } from '../_helpers'
import { Accordion as AccordionBase, Chat } from '../common'

import CuratorSection from './CuratorSection'
import DataType from './DataType'
import DecisionSection from './DecisionSection'
import EditorPanelRibbon from './EditorPanelRibbon'
import HistorySection from './HistorySection'
import InfoSection from './InfoSection'
import MetadataSection from './MetadataSection'
import ReviewsSection from './ReviewsSection'

const Wrapper = styled.div`
  padding: ${grid(1)} 0;
`

const AccordionContent = styled.div`
  margin: ${grid(1)} 0 ${grid(1)} ${grid(4)};
`

const Accordion = props => {
  /* eslint-disable-next-line react/prop-types */
  const { children, label, startExpanded } = props

  return (
    <AccordionBase label={label} startExpanded={startExpanded}>
      <AccordionContent>{children}</AccordionContent>
    </AccordionBase>
  )
}

const EditorPanel = props => {
  const {
    acceptedReviewersCount,
    articleTitle,
    invitedReviewersCount,
    rejectedReviewersCount,
    articleUrl,
    authorEmail,
    authorName,
    categories,
    createDoi,
    curatorId,
    curatorName,
    curatorReview,
    dataType,
    dbReferenceId,
    decision,
    decisionLetter,
    decisionSubmitted,
    doi,
    editorName,
    finalizeDoi,
    getSavedDecision,
    getSavedSOChat,
    history,
    onClickCuratorChat,
    onClickReviewerChat,
    onClickReviewerReinvite,
    onClickSetDataType,
    onClickTeamManager,
    pmcId,
    pmId,
    proofLink,
    reviewersFromPreviousVersions,
    reviewersPageUrl,
    reviews,
    reviewExists,
    reviseQualifier,
    saveDecision,
    saveSOChat,
    scienceOfficerChatMessages,
    scienceOfficerName,
    sectionEditorName,
    sendScienceOfficerChatMessage,
    showCuratorChat,
    showCurators,
    showDataType,
    showDataTypeSelection,
    showDecision,
    showHistory,
    showInfo,
    showManageReviewers,
    showMetadata,
    showPreviousReviewers,
    showReviewersChat,
    showReviews,
    showScienceOfficerChat,
    showTeamManager,
    species,
    startChatExpanded,
    startCuratorsExpanded,
    startDataTypeExpanded,
    startDecisionExpanded,
    startHistoryExpanded,
    startMetadataExpanded,
    startReviewsExpanded,
    submissionTypes,
    submitDecision,
    updateMetadata,
  } = props

  return (
    <Wrapper>
      <EditorPanelRibbon dataType={dataType} decision={decision} />

      {showInfo && (
        <InfoSection
          authorEmail={authorEmail}
          authorName={authorName}
          curatorName={curatorName}
          editorName={editorName}
          onClickTeamManager={onClickTeamManager}
          scienceOfficerName={scienceOfficerName}
          sectionEditorName={sectionEditorName}
          showTeamManager={showTeamManager}
        />
      )}

      {showDataType && (
        <Accordion label="Datatype" startExpanded={startDataTypeExpanded}>
          <DataType
            dataType={dataType}
            onClickSetDataType={onClickSetDataType}
            showDataTypeSelection={showDataTypeSelection}
          />
        </Accordion>
      )}

      {showScienceOfficerChat && (
        <Accordion
          label="Chat / Suggest Reviewer"
          startExpanded={startChatExpanded}
        >
          <Chat
            getSavedChat={getSavedSOChat}
            messages={scienceOfficerChatMessages}
            saveChat={saveSOChat}
            sendMessage={sendScienceOfficerChatMessage}
          />
        </Accordion>
      )}

      {showCurators && (
        <Accordion label="Curator" startExpanded={startCuratorsExpanded}>
          <CuratorSection
            curatorId={curatorId}
            curatorName={curatorName}
            onClickChat={onClickCuratorChat}
            reviews={curatorReview}
            showChat={showCuratorChat}
          />
        </Accordion>
      )}

      {showReviews && (
        <Accordion label="Reviews" startExpanded={startReviewsExpanded}>
          <ReviewsSection
            acceptedReviewersCount={acceptedReviewersCount}
            invitedReviewersCount={invitedReviewersCount}
            onClickChat={onClickReviewerChat}
            onClickInvite={onClickReviewerReinvite}
            rejectedReviewersCount={rejectedReviewersCount}
            reviewersFromPreviousVersions={reviewersFromPreviousVersions}
            reviewersPageUrl={reviewersPageUrl}
            reviews={reviews}
            showChat={showReviewersChat}
            showManageReviewers={showManageReviewers}
            showPreviousReviewers={showPreviousReviewers}
          />
        </Accordion>
      )}

      {showDecision && (
        <Accordion label="Decision" startExpanded={startDecisionExpanded}>
          <DecisionSection
            articleTitle={articleTitle}
            articleUrl={articleUrl}
            authorName={authorName}
            decision={decision}
            decisionLetter={decisionLetter}
            finalizeDoi={finalizeDoi}
            getSavedDecision={getSavedDecision}
            proofLink={proofLink}
            reviewExists={reviewExists}
            reviseQualifier={reviseQualifier}
            saveDecision={saveDecision}
            submitDecision={submitDecision}
            submitted={decisionSubmitted}
          />
        </Accordion>
      )}

      {showMetadata && (
        <Accordion label="Metadata" startExpanded={startMetadataExpanded}>
          <MetadataSection
            categories={categories}
            createDoi={createDoi}
            dbReferenceId={dbReferenceId}
            doi={doi}
            finalizeDoi={finalizeDoi}
            pmcId={pmcId}
            pmId={pmId}
            species={species}
            submissionTypes={submissionTypes}
            updateMetadata={updateMetadata}
          />
        </Accordion>
      )}

      {showHistory && (
        <Accordion label="History" startExpanded={startHistoryExpanded}>
          <HistorySection history={history} />
        </Accordion>
      )}
    </Wrapper>
  )
}

const validDecisions = ['accept', 'reject', 'revise', 'publish']

EditorPanel.propTypes = {
  /** Datatype selected for this submission */
  dataType: PropTypes.string,

  /** How many reviewers have been invited so far */
  invitedReviewersCount: PropTypes.number.isRequired,
  /** How many reviewers have accepted their invitation so far */
  acceptedReviewersCount: PropTypes.number.isRequired,
  /** How many reviewers have rejected their invitation so far */
  rejectedReviewersCount: PropTypes.number.isRequired,

  /** Title of article */
  articleTitle: PropTypes.string,
  /** Url for the article */
  articleUrl: PropTypes.string,
  /** Decision that has been taken on this version */
  decision: PropTypes.oneOf(validDecisions),
  /** The decision letter that was sent to the author */
  decisionLetter: PropTypes.string,
  /** Whether the decision has been sent (as opposed to simply saved) */
  decisionSubmitted: PropTypes.bool,
  /** Function to publish the DOI metadata at DataCite */
  finalizeDoi: PropTypes.func,
  /** Link for the proof */
  proofLink: PropTypes.string,
  /** Whether a review has been submitted yet */
  reviewExists: PropTypes.bool,
  /** Qualifier for the revise review decision */
  reviseQualifier: PropTypes.string,

  /** Reference ID from organism database */
  dbReferenceId: PropTypes.string,
  /** DOI given to the manuscript */
  doi: PropTypes.string,
  /** Function to generate new DOI */
  createDoi: PropTypes.func,
  /** Categories for the manuscript */
  categories: PropTypes.arrayOf(PropTypes.string),
  /** PubMed ID for the manuscript */
  pmId: PropTypes.string,
  /** PMC ID for the manuscript */
  pmcId: PropTypes.string,
  /** Species for the manuscript */
  species: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string)),
  /** Submission Type */
  submissionTypes: PropTypes.arrayOf(PropTypes.string),

  /** Dates of events for the manuscript */
  history: PropTypes.shape({
    received: PropTypes.string,
    sentForReview: PropTypes.string,
    reviewReceived: PropTypes.string,
    revisionReceived: PropTypes.string,
    accepted: PropTypes.string,
    published: PropTypes.string,
  }),

  /** Submitting author's email */
  authorEmail: PropTypes.string,
  /** Submitting author's display name */
  authorName: PropTypes.string,
  /** Assigned curator's display name */
  curatorName: PropTypes.arrayOf(PropTypes.string),
  /** Assigned editor's display name */
  editorName: PropTypes.string,

  /** Assigned section editor's display name */
  sectionEditorName: PropTypes.string,
  /** Assigned science officer's display name */
  scienceOfficerName: PropTypes.string,

  /** Function to run on clicking 'chat with curator' */
  onClickCuratorChat: PropTypes.func,
  /** Function to run on clicking 'chat with reviewer' */
  onClickReviewerChat: PropTypes.func,
  /** Function to run on clicking 'reinvite reviewer' */
  onClickReviewerReinvite: PropTypes.func,
  /** Function to run on clicking 'set datatype' */
  onClickSetDataType: PropTypes.func,
  /** Function to run on clicking 'team manager' */
  onClickTeamManager: PropTypes.func,
  /** Function to run on clicking 'send to author' in the decision section */
  submitDecision: PropTypes.func,
  /** Function to run on clicking 'update metadata' */
  updateMetadata: PropTypes.func,

  /** List of reviewers that existed on previous versions */
  reviewersFromPreviousVersions: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      displayName: PropTypes.string,
      recommendation: PropTypes.oneOf(validDecisions),
    }),
  ),

  /** URL of 'manage reviewers' page */
  reviewersPageUrl: PropTypes.string,

  /** Pending or submitted reviews for this version */
  reviews: PropTypes.arrayOf(
    PropTypes.shape({
      askedToSeeRevision: PropTypes.bool,
      content: PropTypes.string,
      openAcknowledgement: PropTypes.bool,
      pending: PropTypes.bool,
      recommendation: PropTypes.oneOf(validDecisions),
      reviewerId: PropTypes.string,
      reviewerName: PropTypes.string,
    }),
  ),

  /** Curator's user id */
  curatorId: PropTypes.string,

  /** Curator's review */
  curatorReview: PropTypes.shape({
    content: PropTypes.string,
    openAcknowledgement: PropTypes.bool,
    submitted: PropTypes.string,
    recommendation: PropTypes.oneOf(validDecisions),
  }),

  /** Fetch saved (but not submitted) decision */
  getSavedDecision: PropTypes.func,
  /** Save decision without submitting */
  saveDecision: PropTypes.func,
  /** Save science officer chat without sending */
  saveSOChat: PropTypes.func,
  /** Fetch saved but not sent science officer chat */
  getSavedSOChat: PropTypes.func,

  /** Science officer chat messages */
  scienceOfficerChatMessages: PropTypes.arrayOf(
    PropTypes.shape({
      displayName: PropTypes.string.isRequired,
      content: PropTypes.string.isRequired,
    }),
  ),

  /** Function to run on clicking 'send' in SO chat */
  sendScienceOfficerChatMessage: PropTypes.func,

  /** Control whether the info section at the top is shown or not */
  showInfo: PropTypes.bool,
  /** Control whether to show the datatype section */
  showDataType: PropTypes.bool,
  /** Control whether the science officer chat is shown or not */
  showScienceOfficerChat: PropTypes.bool,
  /** Control whether the curator section is show or not */
  showCurators: PropTypes.bool,
  /** Control whether the reviews section is shown or not */
  showReviews: PropTypes.bool,
  /** Control whether the decision section is shown or not */
  showDecision: PropTypes.bool,
  /** Control whether the metadata section is shown or not */
  showMetadata: PropTypes.bool,
  /** Control whether the history section is shown or not */
  showHistory: PropTypes.bool,

  /** Control whether to show the datatype dropdown when there is no value */
  showDataTypeSelection: PropTypes.bool,

  /** Control whether the curator chat is shown or not */
  showCuratorChat: PropTypes.bool,
  /** Control whether to show the previous reviewers list in the reviewers section */
  showPreviousReviewers: PropTypes.bool,
  /** Control whether the 'chat with reviewer' buttons in the reviews section are shown or not */
  showReviewersChat: PropTypes.bool,
  /** Control whether the 'Manage reviewers' link is displayed in the reviews section */
  showManageReviewers: PropTypes.bool,
  /** Control whether the 'manage team' button in the info section is shown or not */
  showTeamManager: PropTypes.bool,

  /** Control whether the science officer chat should start expanded */
  startChatExpanded: PropTypes.bool,
  /** Control whether the curator section should start expanded */
  startCuratorsExpanded: PropTypes.bool,
  /** Control whether the datatype section should start expanded */
  startDataTypeExpanded: PropTypes.bool,
  /** Control whether the reviews section should start expanded */
  startReviewsExpanded: PropTypes.bool,
  /** Control whether the decision section should start expanded */
  startDecisionExpanded: PropTypes.bool,
  /** Control whether the metadata section should start expanded */
  startMetadataExpanded: PropTypes.bool,
  /** Control whether the history section should start expanded */
  startHistoryExpanded: PropTypes.bool,
}

EditorPanel.defaultProps = {
  articleTitle: null,
  articleUrl: null,
  authorEmail: null,
  authorName: null,
  categories: null,
  createDoi: null,
  curatorId: null,
  curatorName: null,
  curatorReview: null,
  dataType: null,
  dbReferenceId: null,
  decision: null,
  decisionLetter: null,
  decisionSubmitted: false,
  doi: null,
  editorName: null,
  finalizeDoi: null,
  getSavedDecision: null,
  getSavedSOChat: null,
  history: null,
  onClickCuratorChat: null,
  onClickReviewerChat: null,
  onClickReviewerReinvite: null,
  onClickSetDataType: null,
  onClickTeamManager: null,
  pmId: null,
  pmcId: null,
  proofLink: null,
  reviewersFromPreviousVersions: [],
  reviewersPageUrl: null,
  reviews: [],
  reviewExists: false,
  reviseQualifier: null,
  saveDecision: null,
  saveSOChat: null,
  scienceOfficerName: null,
  scienceOfficerChatMessages: [],
  sectionEditorName: null,
  sendScienceOfficerChatMessage: null,
  showCuratorChat: false,
  showCurators: false,
  showDataType: false,
  showDataTypeSelection: false,
  showDecision: false,
  showInfo: false,
  showHistory: false,
  showManageReviewers: false,
  showMetadata: false,
  showPreviousReviewers: false,
  showReviewersChat: false,
  showReviews: false,
  showScienceOfficerChat: false,
  showTeamManager: false,
  species: null,
  startChatExpanded: false,
  startCuratorsExpanded: false,
  startDataTypeExpanded: false,
  startDecisionExpanded: false,
  startHistoryExpanded: false,
  startMetadataExpanded: false,
  startReviewsExpanded: false,
  submissionTypes: null,
  submitDecision: null,
  updateMetadata: null,
}

export default EditorPanel
