import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { Button } from '../common'
import ValueList from './ValueList'

const Wrapper = styled.div`
  display: flex;
  width: 100%;
`

const TeamManagerWrapper = styled.div`
  align-items: flex-end;
  display: flex;
  flex-grow: 1;
  justify-content: flex-end;
`

const InfoSection = props => {
  const {
    authorEmail,
    authorName,
    curatorName,
    editorName,
    sectionEditorName,
    scienceOfficerName,
    onClickTeamManager,
    showTeamManager,
  } = props

  const teamValues = [
    {
      label: 'Main Editor',
      status: 'primary',
      value: editorName,
    },
    {
      label: 'Section Editor',
      status: 'primary',
      value: sectionEditorName,
    },
    {
      label: 'Science Officer',
      status: 'primary',
      value: scienceOfficerName,
    },
    {
      label: 'Curator',
      status: 'primary',
      value: curatorName && curatorName.join(', '),
    },
    {
      label: 'Submitting Author',
      status: 'primary',
      value: `${authorName} ( ${authorEmail} )`,
    },
  ]

  return (
    <Wrapper>
      <ValueList data={teamValues} missingValueText="not assigned" />

      {showTeamManager && (
        <TeamManagerWrapper>
          <Button onClick={onClickTeamManager} primary>
            Manage Team
          </Button>
        </TeamManagerWrapper>
      )}
    </Wrapper>
  )
}

InfoSection.propTypes = {
  authorEmail: PropTypes.string.isRequired,
  authorName: PropTypes.string.isRequired,
  curatorName: PropTypes.arrayOf(PropTypes.string),
  editorName: PropTypes.string,
  sectionEditorName: PropTypes.string,
  scienceOfficerName: PropTypes.string,
  onClickTeamManager: PropTypes.func,
  showTeamManager: PropTypes.bool,
}

InfoSection.defaultProps = {
  curatorName: null,
  editorName: null,
  sectionEditorName: null,
  scienceOfficerName: null,
  onClickTeamManager: null,
  showTeamManager: false,
}

export default InfoSection
