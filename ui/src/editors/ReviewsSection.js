import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { grid } from '../_helpers'
import { Action, RibbonFeedback } from '../common'

import ReinviteReviewerList from './ReinviteReviewerList'
import ReviewList from './ReviewList'
import ReviewersNumbers from './ReviewersNumbers'

const Feedback = styled(RibbonFeedback)`
  margin-bottom: 0;
`

const TopRow = styled.div`
  display: flex;

  > div:first-child {
    flex-grow: 1;
  }
`

const Wrapper = styled.div`
  > div:not(:first-child):not(:last-child) {
    margin-bottom: ${grid(3)};
  }
`

const LinkWrapper = styled.div``

const ReviewsSection = props => {
  const {
    acceptedReviewersCount,
    invitedReviewersCount,
    onClickChat,
    onClickInvite,
    rejectedReviewersCount,
    reviews,
    reviewersFromPreviousVersions,
    reviewersPageUrl,
    showChat,
    showManageReviewers,
    showPreviousReviewers,
  } = props

  return (
    <Wrapper>
      <Feedback
        keepSpaceOccupied={showPreviousReviewers}
        successMessage="Reviewer has been successfully reinivited"
      >
        {notifyRibbon => {
          const reinvite = reviewerId =>
            onClickInvite(reviewerId).then(() => notifyRibbon(true))

          return (
            <>
              <TopRow>
                <ReviewersNumbers
                  accepted={acceptedReviewersCount}
                  invited={invitedReviewersCount}
                  rejected={rejectedReviewersCount}
                />

                {showManageReviewers && (
                  <LinkWrapper>
                    <Action to={reviewersPageUrl}>Manage Reviewers</Action>
                  </LinkWrapper>
                )}
              </TopRow>

              {showPreviousReviewers && (
                <ReinviteReviewerList
                  onClickInvite={reinvite}
                  reviewers={reviewersFromPreviousVersions}
                />
              )}

              <ReviewList
                onClickChat={onClickChat}
                reviews={reviews}
                showChat={showChat}
              />
            </>
          )
        }}
      </Feedback>
    </Wrapper>
  )
}

ReviewsSection.propTypes = {
  acceptedReviewersCount: PropTypes.number.isRequired,
  invitedReviewersCount: PropTypes.number.isRequired,
  rejectedReviewersCount: PropTypes.number.isRequired,
  onClickChat: PropTypes.func.isRequired,
  onClickInvite: PropTypes.func.isRequired,
  reviewersFromPreviousVersions: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      displayName: PropTypes.string,
      recommendation: PropTypes.oneOf(['accept', 'reject', 'revise']),
    }),
  ),
  reviewersPageUrl: PropTypes.string.isRequired,
  reviews: PropTypes.arrayOf(
    PropTypes.shape({
      askedToSeeRevision: PropTypes.bool,
      content: PropTypes.string,
      openAcknowledgement: PropTypes.bool,
      pending: PropTypes.bool,
      recommendation: PropTypes.oneOf(['accept', 'reject', 'revise']),
      reviewerId: PropTypes.string,
      reviewerName: PropTypes.string,
    }),
  ),
  showChat: PropTypes.bool,
  showManageReviewers: PropTypes.bool,
  showPreviousReviewers: PropTypes.bool,
}

ReviewsSection.defaultProps = {
  reviewersFromPreviousVersions: [],
  reviews: [],
  showChat: false,
  showManageReviewers: false,
  showPreviousReviewers: false,
}

export default ReviewsSection
