import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { useQuery, useMutation } from '@apollo/react-hooks'
import config from 'config'
import { first } from 'lodash'

import {
  EDITOR_PANEL,
  FULL_PREVIEW,
  REINVITE_REVIEWER,
  SEND_CHAT,
  SET_DATA_TYPE,
  SUBMIT_DECISION,
  UPDATE_MANUSCRIPT_METADATA,
  UPDATE_MANUSCRIPT_TEAMS,
} from '../../graphql'

import {
  ChatModal,
  DataTypeConfirmation,
  DateParser,
  EditorPanel,
  ManuscriptTeamManager,
  SyncedTabs,
} from '../../../ui'

import { ArticlePreview } from '../../components/ui'

import { exportManuscriptToPrint } from '../../fetch/exportManuscript'
import transform from '../_helpers/transformEditorPanelData'
import {
  getFromStorage,
  transformChatMessages,
  saveToStorage,
} from '../_helpers/common'
import { createDoi, publishDoi } from '../../fetch/DataCiteApi'

/* eslint-disable-next-line react/prop-types */
const Label = ({ created, index }) => (
  <DateParser dateFormat="MM.DD.YY HH:mm" timestamp={new Date(Number(created))}>
    {timestamp => (
      <span>
        {index === 0
          ? `Original: ${timestamp}`
          : `Revision ${index}: ${timestamp}`}
      </span>
    )}
  </DateParser>
)

const EditorView = props => {
  const { showEditorAssignment, manuscriptId } = props

  /**
   * Handle modals with state
   */
  const [selectedDataType, setSelectedDataType] = useState(null)
  const [showDataTypeConfirm, setShowDataTypeConfirm] = useState(false)

  const [chatModalThreadId, setChatModalThreadId] = useState(null)
  const [chatModalHeader, setChatModalHeader] = useState(null)
  const [showChatModal, setShowChatModal] = useState(false)
  const [showTeamManager, setShowTeamManager] = useState(false)

  const onClickSetDataType = selected => {
    setSelectedDataType(selected)
    setShowDataTypeConfirm(true)
  }

  let chatData, assignedCurator // will be populated further down
  const onClickReviewerChat = reviewerId => {
    const selectedChat = chatData.find(item => item.reviewerId === reviewerId)
    setChatModalThreadId(selectedChat.id)
    setShowChatModal(true)
  }

  const onClickCuratorChat = curatorId => {
    const selectedChat = chatData.find(
      item => item.chatType === 'curator' && item.reviewerId === curatorId,
    )
    setChatModalThreadId(selectedChat.id)
    setChatModalHeader('Chat with Curator')
    setShowChatModal(true)
  }

  /**
   * Queries & mutations
   */

  const { data: previewData, loading: previewLoading } = useQuery(
    FULL_PREVIEW,
    {
      variables: { id: manuscriptId },
    },
  )

  const { data: editorData, loading: editorLoading } = useQuery(EDITOR_PANEL, {
    variables: { id: manuscriptId },
  })

  const queryRefetch = {
    refetchQueries: [
      {
        query: EDITOR_PANEL,
        variables: { id: manuscriptId },
      },
    ],
  }

  const [reinviteReviewerMutation] = useMutation(
    REINVITE_REVIEWER,
    queryRefetch,
  )
  const [sendChatMutation] = useMutation(SEND_CHAT, {
    refetchQueries: [
      {
        query: EDITOR_PANEL,
        variables: { id: manuscriptId },
      },
      {
        query: FULL_PREVIEW,
        variables: { id: manuscriptId },
      },
    ],
  })
  const [setDataTypeMutation] = useMutation(SET_DATA_TYPE, queryRefetch)
  const [submitDecisionMutation] = useMutation(SUBMIT_DECISION, {
    refetchQueries: [
      {
        query: EDITOR_PANEL,
        variables: { id: manuscriptId },
      },
      {
        query: FULL_PREVIEW,
        variables: { id: manuscriptId },
      },
    ],
  })
  const [updateManuscriptTeamsMutation] = useMutation(
    UPDATE_MANUSCRIPT_TEAMS,
    queryRefetch,
  )
  const [updateMetadataMutation] = useMutation(
    UPDATE_MANUSCRIPT_METADATA,
    queryRefetch,
  )

  /**
   * Left side: preview
   */

  let leftSections

  if (
    previewData &&
    previewData.manuscript &&
    previewData.manuscript.versions
  ) {
    // there is only one chat here, the author chat
    const authorChat = previewData.manuscript.chatThreads[0]

    const authorChatMessages = transformChatMessages(authorChat.messages)

    const sendAuthorChatMessage = content =>
      sendChatMutation({
        variables: {
          input: {
            chatThreadId: authorChat.id,
            content,
          },
        },
      })

    const saveAuthorChat = message =>
      saveToStorage(message, `chat_${authorChat.id}`)
    const getSavedAuthorChat = () => getFromStorage(`chat_${authorChat.id}`)

    const filteredVersions = previewData.manuscript.versions.filter(
      v =>
        (!v.submitted &&
          previewData.manuscript.isInitiallySubmitted &&
          !previewData.manuscript.isDataTypeSelected) ||
        v.submitted,
    )

    leftSections = filteredVersions.map((version, index) => ({
      key: version.id,
      label: <Label created={version.created} index={index} />,
      content: (
        <ArticlePreview
          article={version}
          authorChatMessages={authorChatMessages}
          exportManuscript={exportManuscriptToPrint}
          getSavedAuthorChat={getSavedAuthorChat}
          isEditor
          manuscriptId={manuscriptId}
          previousVersion={filteredVersions[index - 1]}
          saveAuthorChat={saveAuthorChat}
          sendAuthorChatMessage={sendAuthorChatMessage}
          showAdditionalData
          showHeader={false}
        />
      ),
    }))
  }

  /**
   * Right side: editor panel
   */

  let rightSections
  let editors, sectionEditors, scienceOfficers, curators
  let assignedEditor, assignedSectionEditor, assignedScienceOfficer
  let sendModalChatMessage, setDataType, updateManuscriptTeams, updateMetadata

  const getSavedChat = () => getFromStorage(`chat_${chatModalThreadId}`)
  const saveChat = content =>
    saveToStorage(content, `chat_${chatModalThreadId}`)
  const finalizeDoi = () => publishDoi(manuscriptId)

  if (!editorLoading && editorData) {
    const transformed = transform(editorData, {
      finalizeDoi,
      reinviteReviewerMutation,
      sendChatMutation,
      setDataTypeMutation,
      submitDecisionMutation,
      updateManuscriptTeamsMutation,
      updateMetadataMutation,
    })

    ;({
      assignedCurator,
      assignedEditor,
      assignedSectionEditor,
      assignedScienceOfficer,

      editors,
      sectionEditors,
      scienceOfficers,
      curators,

      chatData,

      setDataType,
      updateManuscriptTeams,
      updateMetadata,
    } = transformed)

    const {
      categories,
      dbReferenceId,
      doi,
      pmId,
      pmcId,
      history,
      reinviteReviewer: reinviteReviewerFn,
      sendChatMessage,
      species,
      submissionTypes,
      submitDecision: submitDecisionFn,
      versions,
    } = transformed

    const scienceOfficerChat = chatData.find(
      thread => thread.chatType === 'scienceOfficer',
    )
    const scienceOfficerChatMessages = scienceOfficerChat.messages

    const sendScienceOfficerChatMessage = content =>
      sendChatMessage(content, scienceOfficerChat.id)

    const getSavedSOChat = () => getFromStorage(`chat_${scienceOfficerChat.id}`)
    const saveSOChat = message =>
      saveToStorage(message, `chat_${scienceOfficerChat.id}`)

    sendModalChatMessage = content =>
      sendChatMessage(content, chatModalThreadId)

    rightSections = versions.map((version, index) => {
      const {
        authorEmail,
        authorName,
        created,
        dataType,
        decision,
        decisionLetter,
        id,
        latest,
        title,

        invitedReviewersCount,
        acceptedReviewersCount,
        rejectedReviewersCount,

        curatorReview,
        reviews,
        previousReviewers,
      } = version

      const getSavedDecision = () =>
        JSON.parse(getFromStorage(`decision_${id}`))
      const saveDecision = message =>
        saveToStorage(JSON.stringify(message), `decision_${id}`)

      const editorName = assignedEditor && assignedEditor.displayName
      const curatorId = assignedCurator && assignedCurator.id
      const curatorNames =
        assignedCurator && assignedCurator.map(c => c.displayName)
      const sectionEditorName =
        assignedSectionEditor && assignedSectionEditor.displayName
      const scienceOfficerName =
        assignedScienceOfficer && assignedScienceOfficer.displayName

      const reinviteReviewer = reviewerId =>
        reinviteReviewerFn(version.id, reviewerId)
      const submitDecision = input => submitDecisionFn(version.id, input)

      const { pdfUrl, baseUrl } = config['pubsweet-client']
      const printLink = `${baseUrl}/api/export/${version.id}/print`
      const proofLink = pdfUrl ? `${pdfUrl}${printLink}` : printLink
      const articleUrl = `${baseUrl}/article/${manuscriptId}`

      const reviewExists = reviews.some(review => !review.pending)

      const reviseQualifier = first(
        reviews
          .filter(review => review.reviseQualifier)
          .map(review => review.reviseQualifier),
      )

      return {
        key: id,
        label: <Label created={created} index={index} />,
        content: (
          <EditorPanel
            acceptedReviewersCount={acceptedReviewersCount}
            articleTitle={title}
            articleUrl={articleUrl}
            authorEmail={authorEmail}
            authorName={authorName}
            categories={categories}
            createDoi={createDoi}
            curatorId={curatorId}
            curatorName={curatorNames}
            curatorReview={curatorReview}
            dataType={dataType}
            dbReferenceId={dbReferenceId}
            decision={decision}
            decisionLetter={decisionLetter}
            decisionSubmitted={!!decision}
            doi={doi}
            editorName={editorName}
            finalizeDoi={finalizeDoi}
            getSavedDecision={getSavedDecision}
            getSavedSOChat={getSavedSOChat}
            history={history}
            invitedReviewersCount={invitedReviewersCount}
            key={version.id}
            onClickCuratorChat={onClickCuratorChat}
            onClickReviewerChat={onClickReviewerChat}
            onClickReviewerReinvite={reinviteReviewer}
            onClickSetDataType={onClickSetDataType}
            onClickTeamManager={() => setShowTeamManager(true)}
            pmcId={pmcId}
            pmId={pmId}
            proofLink={proofLink}
            rejectedReviewersCount={rejectedReviewersCount}
            reviewersFromPreviousVersions={previousReviewers}
            reviewersPageUrl={`/assign-reviewers/${manuscriptId}`}
            reviewExists={reviewExists}
            reviews={reviews}
            reviseQualifier={reviseQualifier}
            saveDecision={saveDecision}
            saveSOChat={saveSOChat}
            scienceOfficerChatMessages={scienceOfficerChatMessages}
            scienceOfficerName={scienceOfficerName}
            sectionEditorName={sectionEditorName}
            sendScienceOfficerChatMessage={sendScienceOfficerChatMessage}
            showCuratorChat
            showCurators
            showDataType={latest}
            showDataTypeSelection
            showDecision
            showHistory={latest}
            showInfo={latest}
            showManageReviewers={latest}
            showMetadata={latest}
            showPreviousReviewers={latest && versions.length > 1}
            showReviewersChat
            showReviews={dataType}
            showScienceOfficerChat={latest}
            showTeamManager
            species={species}
            // startChatExpanded
            // startCuratorsExpanded
            startDataTypeExpanded={!dataType}
            // startDecisionExpanded
            // startMetadataExpanded
            // startReviewsExpanded
            submissionTypes={submissionTypes}
            submitDecision={submitDecision}
            updateMetadata={updateMetadata}
          />
        ),
      }
    })
  }

  /**
   * Render
   */

  return (
    <>
      <SyncedTabs
        leftHeader="Article Preview"
        leftLoading={previewLoading}
        leftSections={leftSections}
        rightHeader="Editor Panel"
        rightLoading={editorLoading}
        rightSections={rightSections}
      />

      {/* Modals */}

      {!editorLoading && (
        <ManuscriptTeamManager
          assignedCurator={assignedCurator}
          assignedEditor={assignedEditor}
          assignedScienceOfficer={assignedScienceOfficer}
          assignedSectionEditor={assignedSectionEditor}
          curators={curators}
          editors={editors}
          // isOpen
          isOpen={showTeamManager}
          onRequestClose={() => setShowTeamManager(false)}
          scienceOfficers={scienceOfficers}
          sectionEditors={sectionEditors}
          showEditorAssignment={showEditorAssignment}
          updateTeams={updateManuscriptTeams}
        />
      )}

      <DataTypeConfirmation
        dataTypeLabel={selectedDataType && selectedDataType.label}
        isOpen={showDataTypeConfirm}
        onConfirm={() => {
          setDataType(selectedDataType.value).then(() => {
            setShowDataTypeConfirm(false)
          })
        }}
        onRequestClose={() => setShowDataTypeConfirm(false)}
      />

      {chatModalThreadId && (
        <ChatModal
          getSavedChat={getSavedChat}
          headerText={chatModalHeader}
          isOpen={showChatModal}
          messages={
            chatData.find(thread => thread.id === chatModalThreadId).messages
          }
          onRequestClose={() => {
            setShowChatModal(false)
            setChatModalHeader(null)
          }}
          saveChat={saveChat}
          sendMessage={sendModalChatMessage}
        />
      )}
    </>
  )
}

EditorView.propTypes = {
  manuscriptId: PropTypes.bool.isRequired,
  showEditorAssignment: PropTypes.bool.isRequired,
}

export default EditorView
