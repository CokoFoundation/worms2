// /* eslint-disable react/prop-types */

// import React from 'react'
// import { Query } from '@apollo/react-components'
// import gql from 'graphql-tag'

// const GET_ARTICLE_FOR_EDITOR = gql`
//   query GetArticleForEditor($id: ID!) {
//     manuscript(id: $id) {
//       chatThreads {
//         id
//         chatType
//         messages {
//           content
//           timestamp
//           user {
//             displayName
//           }
//         }
//         userId
//       }
//       currentlyWith
//       doi
//       id
//       isDataTypeSelected
//       isInitiallySubmitted
//       teams {
//         id
//         role
//         members {
//           id
//           user {
//             id
//             displayName
//           }
//         }
//       }
//       versions {
//         id
//         created
//         active
//         isApprovedByScienceOfficer
//         submitted
//         authors {
//           email
//           firstName
//           lastName
//           submittingAuthor
//         }
//         decision
//         decisionLetter
//         reviews {
//           id
//           content
//           openAcknowledgement
//           recommendation
//           reviewer {
//             id
//             displayName
//           }
//           askedToSeeRevision
//           status {
//             pending
//             submitted
//           }
//         }
//         teams {
//           id
//           role
//           members {
//             id
//             status
//             user {
//               id
//               displayName
//             }
//           }
//         }
//       }
//     }
//   }
// `

// const GetArticleForEditorQuery = props => {
//   const { articleId: id, render } = props

//   return (
//     <Query query={GET_ARTICLE_FOR_EDITOR} variables={{ id }}>
//       {render}
//     </Query>
//   )
// }

// export { GET_ARTICLE_FOR_EDITOR }
// export default GetArticleForEditorQuery
