/* eslint-disable react/prop-types */

import React from 'react'
import styled from 'styled-components'
import { Toggle } from 'react-powerplug'

import { Action, ActionGroup } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'

import { isEditableByAuthor } from '../../helpers/status'
import SectionItemWithStatus from './SectionItemWithStatus'
import ConfirmManuscriptDelete from '../../../ui/src/modals/ConfirmManuscriptDelete'

const Wrapper = styled.div`
  border-bottom: ${th('borderWidth')} ${th('borderStyle')}
    ${th('colorFurniture')};
  /* margin-bottom: ${th('gridUnit')}; */
  padding: 4px 0;
`

const ActionsWrapper = styled.div`
  flex-shrink: 0;
`

const Actions = props => {
  const { articleId, deleteArticle, status, title } = props
  const isEditable = isEditableByAuthor(status)
  const confirmDelete = () => {
    deleteArticle({ variables: { id: articleId } })
  }

  return (
    <ActionsWrapper>
      <ActionGroup>
        <Action
          data-test-id={`author-action-edit-${articleId}`}
          to={`/article/${articleId}`}
        >
          {isEditable ? 'Edit' : 'View Article'}
        </Action>

        {isEditable && (
          <Toggle intial={false}>
            {({ on, toggle }) => (
              <React.Fragment>
                <Action onClick={toggle} primary>
                  Delete
                </Action>

                <ConfirmManuscriptDelete
                  isOpen={on}
                  onConfirm={confirmDelete}
                  onRequestClose={toggle}
                  title={title}
                />
              </React.Fragment>
            )}
          </Toggle>
        )}
      </ActionGroup>
    </ActionsWrapper>
  )
}

const AuthorSectionItem = props => {
  const { deleteArticle, id, status, title } = props

  const ActionsComponent = (
    <Actions
      articleId={id}
      deleteArticle={deleteArticle}
      status={status}
      title={title}
    />
  )

  return (
    <Wrapper data-test-id="dashboard-section-item-author">
      <SectionItemWithStatus actionsComponent={ActionsComponent} {...props} />
    </Wrapper>
  )
}

export default AuthorSectionItem
