import React from 'react'
import ConfigurableEditor from '../configurable/ConfigurableEditor'

const AbstractEditor = props => (
  <ConfigurableEditor
    bold
    heading
    italic
    link
    smallcaps
    subscript
    superscript
    underline
    {...props}
  />
)

export default AbstractEditor
